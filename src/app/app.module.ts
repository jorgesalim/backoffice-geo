import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './page/login/login.component';
import { FooterComponent } from './common/footer/footer.component';
import { HeaderComponent } from './common/header/header.component';
import { HomeComponent } from './page/home/home.component';
import { PageNotFoundComponent } from './common/page-not-found/page-not-found.component';
import { AttendanceMapsComponent } from './page/attendance-maps/attendance-maps.component';
import { AttendanceListComponent } from './page/attendance-list/attendance-list.component';
import { RegistrationComponent } from './page/registration/registration.component';
import { ReportComponent } from './page/report/report.component';
import { HelpComponent } from './page/help/help.component';
import {AgmCoreModule} from '@agm/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserService} from './services/user.service';
import {BsDropdownModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {AppBoostrapModule} from './app-boostrap/app-bootstrap.module';
import { GeoFormComponent } from './page/geo-form/geo-form.component';
import { EmpresaComponent } from './page/cadastro/empresa/empresa.component';
import { EmpreiteiraComponent } from './page/cadastro/empreiteira/empreiteira.component';
import { VistoriaComponent } from './page/cadastro/vistoria/vistoria.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    FooterComponent,
    HeaderComponent,
    HomeComponent,
    PageNotFoundComponent,
    AttendanceMapsComponent,
    AttendanceListComponent,
    RegistrationComponent,
    ReportComponent,
    HelpComponent,
    GeoFormComponent,
    EmpresaComponent,
    EmpreiteiraComponent,
    VistoriaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppBoostrapModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC5TvqWT-CqOV12BR8ccOWr8x_ooU8WdUc'
    })
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
