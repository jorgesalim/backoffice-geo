import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {User} from '../model/user';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private API = 'http://52.207.213.76:1337/users';

  constructor(private http: HttpClient) { }

  login(identifier: string, password: string) {

    return

    const options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      }),
       body: {'identifier': identifier, 'password': password}};
    return this.http.get<User>(this.API, options)
      .pipe(
        map(user => {
          if (user[0].confirmed) {
            localStorage.setItem('user', JSON.stringify(user[0]));
          }
          return user[0];
        }),
        delay(2000),
        tap(console.log),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }
}
