import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Empresa} from '../model/empresa';
import {Empreiteira} from '../model/empreiteira';
import { map } from '../../../node_modules/rxjs/operators';
import {Obra} from '../model/obra';

@Injectable({
  providedIn: 'root'
})
export class EmpresaService {

  constructor(private http: HttpClient) { }

  getEmpresas() {
    return this.http.get<Empresa[]>('assets/dados/empresas.json');
  }

  getEmpreiteiras(idEmpresa: number) {
    return this.http.get<Empreiteira[]>('assets/dados/empreiteiras.json')
      .pipe(
        // tslint:disable-next-line:triple-equals
        map((empreiteiras: Empreiteira[]) => empreiteiras.filter(e => e.idEmpresa == idEmpresa))
      );
  }

  getObras(idEmpreiteira: number) {
    return this.http.get<Obra[]>('assets/dados/obras.json')
      .pipe(
        // tslint:disable-next-line:triple-equals
        map((obras: Obra[]) => obras.filter(e => e.idEmpreiteira == idEmpreiteira))
      );
  }

  getObra(idObra: number) {
    return this.http.get<Obra[]>('assets/dados/obras.json')
      .pipe(
        // tslint:disable-next-line:triple-equals
        map((obras: Obra[]) => obras.filter(e => e.id == idObra))
      );
  }
}
