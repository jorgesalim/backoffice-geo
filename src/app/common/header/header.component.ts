import {Component, OnChanges, OnInit} from '@angular/core';
import {LoginComponent} from '../../page/login/login.component';
import {UserService} from '../../services/user.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnChanges {

  constructor(private userService: UserService) { }

  user: User;
  ngOnInit() {
  }

  ngOnChanges() {
  }
}
