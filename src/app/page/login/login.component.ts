import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {UserService} from '../../services/user.service';


import {Router} from '@angular/router';
import {User} from '../../model/user';
import {Role} from '../../model/role';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private service: UserService, private router: Router) { }

  login: any = {
    identifier: '',
    password: ''
  }

  message: string = '';

  private user: User;

  userEmmit = new EventEmitter();

  ngOnInit() {  }

  onSubmit(form) {
    /*
    this.service.login(this.login.identifier, this.login.password)
      .subscribe((newUser) => {
        this.user = newUser;
        this.userEmmit.emit(this.user);
        console.log(this.user.confirmed);
        if (this.user.confirmed) {
          this.router.navigate(['/attendence-maps']);
        } else {
          this.message = 'Usuário ou senhas inválidos';
        }
      });*/
    localStorage.setItem('user', JSON.stringify({
      _id: '1',
      confirmed: true,
      blocked: false,
      username: 'rassis',
      email: 'rassis@geo.com ',
      provider: 'geo',
      role: null,
      __v: 1
    }));
    this.router.navigate(['/attendence-maps']);
  }

}
