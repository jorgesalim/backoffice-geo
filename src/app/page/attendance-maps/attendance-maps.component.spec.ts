import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttendanceMapsComponent } from './attendance-maps.component';

describe('AttendanceMapsComponent', () => {
  let component: AttendanceMapsComponent;
  let fixture: ComponentFixture<AttendanceMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttendanceMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttendanceMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
