import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PageRouterRoutingModule } from './page-router-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PageRouterRoutingModule
  ]
})
export class PageRouterModule { }
