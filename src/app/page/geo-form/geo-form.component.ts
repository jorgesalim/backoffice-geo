import { Component, OnInit } from '@angular/core';
import {Empresa} from '../../model/empresa';
import {map, switchMap, tap} from 'rxjs/operators';
import {Obra} from '../../model/obra';
import {Empreiteira} from '../../model/empreiteira';
import {FormBuilder, FormGroup} from '@angular/forms';
import {EmpresaService} from '../../services/empresa.service';

@Component({
  selector: 'app-geo-form',
  templateUrl: './geo-form.component.html',
  styleUrls: ['./geo-form.component.css']
})
export class GeoFormComponent implements OnInit {

  formulario: FormGroup;

  obra: Obra;
  empresas: Empresa[];
  empreiteiras: Empreiteira[];
  obras: Obra[];
  mapsObras: Obra[];

  constructor(private empresaService: EmpresaService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.empresaService.getEmpresas().subscribe(dados => this.empresas = dados);

    this.formulario = this.formBuilder.group({ empresa: [null], empreiteira: [null], obra: [null] });

    this.formulario.get('empresa').valueChanges
      .pipe(
        tap(empresa => console.log('Nova empresa: ', empresa)),
        map(empresa => this.empresas.filter(e => e.id === Number(empresa))),
        map(empresas => empresas && empresas.length > 0 ? empresas[0].id : null),
        switchMap((empresaId: number) => this.empresaService.getEmpreiteiras(empresaId)),
        tap(console.log)
      )
      .subscribe(empreiteiras => this.empreiteiras = empreiteiras);

    this.formulario.get('empreiteira').valueChanges
      .pipe(
        tap(empreiteira => console.log('Nova empreiteira: ', empreiteira)),
        map(empreiteira => this.empresas.filter(e => e.id === Number(empreiteira))),
        map(empreiteiras => empreiteiras && empreiteiras.length > 0 ? empreiteiras[0].id : null),
        switchMap((empreiteiraId: number) => this.empresaService.getObras(empreiteiraId)),
        tap(console.log)
      )
      .subscribe(obras => this.obras = obras);

    this.formulario.get('obra').valueChanges
      .pipe(
        tap(obra => console.log('Nova obra: ', obra)),
        map(obra => this.obras.filter((e) => e.id === Number(obra))),
        map(empreiteiras => empreiteiras && empreiteiras.length > 0 ? empreiteiras[0].id : null),
        switchMap((obraId: number) => this.empresaService.getObra(obraId)),
        tap(console.log))
      .subscribe(obras => this.mapsObras = obras);
  }
}
