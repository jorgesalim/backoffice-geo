import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AttendanceListComponent} from './attendance-list/attendance-list.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';

import {AttendanceMapsComponent} from './attendance-maps/attendance-maps.component';
import {RegistrationComponent} from './registration/registration.component';
import {ReportComponent} from './report/report.component';
import {HelpComponent} from './help/help.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';

@NgModule({
  declarations: [
    AttendanceListComponent,
    LoginComponent,
    HomeComponent,
    AttendanceMapsComponent,
    AttendanceListComponent,
    RegistrationComponent,
    ReportComponent,
    HelpComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC5TvqWT-CqOV12BR8ccOWr8x_ooU8WdUc'
    })
  ]
})
export class PageModule { }
