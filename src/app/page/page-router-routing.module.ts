import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AttendanceMapsComponent} from './attendance-maps/attendance-maps.component';
import {AttendanceListComponent} from './attendance-list/attendance-list.component';
import {RegistrationComponent} from './registration/registration.component';
import {ReportComponent} from './report/report.component';
import {HelpComponent} from './help/help.component';
import {PageNotFoundComponent} from '../common/page-not-found/page-not-found.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from './login/login.component';
import {PageGuardGuard} from './page-guard.guard';
import {EmpresaComponent} from './cadastro/empresa/empresa.component';
import {EmpreiteiraComponent} from './cadastro/empreiteira/empreiteira.component';
import {VistoriaComponent} from './cadastro/vistoria/vistoria.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'login', component: LoginComponent},
  { path: 'home', component: HomeComponent},
  { path: 'attendence-maps', component: AttendanceMapsComponent, canActivate: [PageGuardGuard]},
  { path: 'attendence-list', component: AttendanceListComponent, canActivate: [PageGuardGuard]},
  { path: 'registration', component: RegistrationComponent, canActivate: [PageGuardGuard]},
  { path: 'empresa', component: EmpresaComponent, canActivate: [PageGuardGuard]},
  { path: 'empreiteira', component: EmpreiteiraComponent, canActivate: [PageGuardGuard]},
  { path: 'vistoria', component: VistoriaComponent, canActivate: [PageGuardGuard]},
  { path: 'report', component: ReportComponent, canActivate: [PageGuardGuard]},
  { path: 'help', component: HelpComponent},
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRouterRoutingModule { }
