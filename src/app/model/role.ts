export class Role {
  _id: string;
  name: string;
  description: string;
  type: string;
  __v: number;
}
