import {Role} from './role';

export class User {
  _id: string;
  confirmed: boolean;
  blocked: boolean;
  username: string;
  email: string;
  provider: string;
  role: Role;
  __v: number;
}
