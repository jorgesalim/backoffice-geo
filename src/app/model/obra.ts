export interface Obra {
  id: number;
  nome: string;
  idEmpreiteira: number;
}
