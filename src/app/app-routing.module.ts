import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageNotFoundComponent} from './common/page-not-found/page-not-found.component';
import {HelpComponent} from './page/help/help.component';
import {ReportComponent} from './page/report/report.component';
import {RegistrationComponent} from './page/registration/registration.component';
import {AttendanceListComponent} from './page/attendance-list/attendance-list.component';
import {AttendanceMapsComponent} from './page/attendance-maps/attendance-maps.component';
import {LoginComponent} from './page/login/login.component';
import {PageRouterModule} from './page/page-router.module';

const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    PageRouterModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
